#!/bin/sh

case "$1" in
    --toggle)
        if [ "$(pgrep -x dropbox)" ]; then
            pkill -f dropbox
        else
            dropbox & 
        fi
        ;;
    *)
        if [ "$(pgrep -x dropbox)" ]; then
            echo ""
        else
            echo "%{F#373B41}"
        fi
        ;;
esac
