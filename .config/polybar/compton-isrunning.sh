#!/bin/sh

case "$1" in
    --toggle)
        if [ "$(pgrep -x compton)" ]; then
            pkill compton
        else
            compton --config $HOME/.config/compton/compton.conf -b &
        fi
        ;;
    *)
        if [ "$(pgrep -x compton)" ]; then
            echo ""
        else
            echo "%{F#373B41}"
        fi
        ;;
esac
