;
; BAR
;

[bar/modern]
width = 100%
height = 35 
offset-x = 0
offset-y = -1 
bottom = true

background = #ff181818
foreground = #e0e0e0

underline-size = 2
underline-color = #c5c6c8

spacing = 1
padding-left = 0
padding-right = 0
module-margin-left = 0 
module-margin-right = 0

locale=ko_KR.UTF8

font-0 = Noto Sans:style=Bold:pixelsize=11
font-1 = Noto Sans CJK KR:style=Bold:pixelsize=11
font-2 = FontAwesome5Free:style=Solid:pixelsize=11
font-3 = FontAwesome5Brands:style=Solid:pixelsize=11

modules-left = bspwm
modules-center = taskwarrior
modules-right = updates-arch-combined dropbox-isrunning redshift-temp-info pulseaudio wireless-network wired-network date powermenu

;tray-position = center
;tray-padding = 2
;tray-offset-x = 35%
;tray-maxsize = 18

wm-restack = bspwm

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;
; MODULES
;

[module/bspwm]
type = internal/bspwm
enable-scroll = false
format = <label-state> <label-mode>
label-focused = %name%
label-focused-background = #ff181818
label-focused-underline= #e0e0e0
label-focused-padding = 4
label-occupied = %name%
label-occupied-padding = 4
label-urgent = %name%
label-urgent-foreground = #cc6666
label-urgent-padding = 4
label-empty = %name% 
label-empty-foreground = #373B41
label-empty-padding = 4

[module/taskwarrior]
type = custom/script
exec = ~/.config/polybar/task-polybar.sh
interval = 30
format = <label>
click-left = task "$((`cat /tmp/tw_polybar_id`))" done

[module/updates-arch-combined]
type = custom/script
exec = ~/.config/polybar/updates-arch-combined.sh
interval = 600
format-padding = 3

[module/dropbox-isrunning]
type = custom/script
exec = ~/.config/polybar/dropbox-isrunning.sh
interval = 30
;click-left = ~/.config/polybar/dropbox-isrunning.sh --toggle
format-padding = 3

[module/redshift-temp-info]
type = custom/script
exec = ~/.config/polybar/redshift-temp-info.sh
interval = 30
format-padding = 3

[module/pulseaudio]
type = internal/pulseaudio
format-volume = <ramp-volume>
format-muted = <label-muted>
label-volume = %percentage% 
label-muted = 
label-muted-foreground = #373B41
format-volume-padding = 3
format-muted-padding = 3
ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

[module/wired-network]
type = internal/network
interface = enp4s0
interval = 3
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-connected = 
label-disconnected = 
format-connected-padding = 3
format-disconnected-padding = 0

[module/wireless-network]
type = internal/network
interface = wlp5s0
interval = 3
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-connected = 
label-disconnected = 
format-connected-padding = 3
format-disconnected-padding = 0

[module/date]
; See "man date" for details on how to format the date string
; NOTE: if you want to use syntax tags here you need to use %%{...}
type = internal/date
interval = 1.0
date = 
time = %I:%M  %p
date-alt = %x (%a)
time-alt = 
label = %date% %time%
format = <label>
format-padding = 3

[module/powermenu]
type = custom/menu

label-open = 
label-close = cancel
label-open-padding = 3
label-close-padding = 3 

menu-0-0 =  
menu-0-0-exec = menu-open-1
menu-0-1 = 
menu-0-1-exec = menu-open-2
menu-0-2 = 
menu-0-2-exec = menu-open-3
menu-0-0-padding = 3
menu-0-1-padding = 3
menu-0-2-padding = 3

menu-1-0 = suspend?  
menu-1-0-exec = systemctl -i suspend
menu-1-0-padding = 3

menu-2-0 = reboot?  
menu-2-0-exec = systemctl -i reboot
menu-2-0-padding = 3

menu-3-0 = power off?  
menu-3-0-exec = systemctl -i poweroff
menu-3-0-padding = 3

;
; INACTIVE
;

[module/padding]
type = custom/text
content = __
content-foreground = #ff181818

[module/memory]
type = internal/memory
interval = 3
format = <label>%
label = %percentage_used%
format-suffix = " M"
format-suffix-foreground = #f0c674 
format-padding = 3

[module/temperature]
type = internal/temperature
units = false
hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input
interval = 3
format = <label>° 
format-warn = <label-warn>°
format-suffix = " C"
format-warn-suffix = " C"
format-suffix-foreground = #81a2be
format-warn-suffix-foreground = #81a2be
label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = #cc6666
format-padding = 3

[module/nvidia-temp]
type = custom/script
exec = nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits
interval = 3
format = <label>°
format-suffix = " G"
format-suffix-foreground = #b5bd68
format-padding = 3

[module/compton-isrunning]
type = custom/script
exec = ~/.config/polybar/compton-isrunning.sh
interval = 30
;click-left = ~/.config/polybar/compton-isrunning.sh --toggle
format-padding = 3

[module/jgmenu]
type = custom/text
content-padding = 3
content = 
click-left = "jgmenu_run >/dev/null 2>&1 &"

; vim:ft=dosini
