set showmatch           " Show matching brackets.
set number              " Show the line numbers on the left side.
set formatoptions+=o    " Continue comment marker in new lines.
set expandtab           " Insert spaces when TAB is pressed.
set tabstop=4           " Render TABs using this many spaces.
set shiftwidth=4        " Indentation amount for < and > commands.
set nojoinspaces        " Prevents inserting two spaces after punctuation on a join (J)


" Plugins will be downloaded under the specified directory.
call plug#begin('~/.config/nvim/plugged')

" Declare the list of plugins.
Plug 'itchyny/lightline.vim'
Plug 'chriskempson/base16-vim'
"Plug 'arcticicestudio/nord-vim'
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Set LightLine Colors
let g:lightline = {
      \ 'colorscheme': 'Tomorrow_Night',
      \ }

" Set colorscheme
let base16colorspace=256
colorscheme base16-tomorrow-night

